# kantner
A detailed description of the project can be found here:  
- `Documentation/Report/KantNER_Report.pdf`

#### how to browse the data
- checkout the folder `TryItOut`
- install any SQL client (I recommend DBeaver: https://dbeaver.io/)
- create a new connection for SQLite and select `TryItOut/namedentities.db` as datasource
- use the SQL-scripts in the folder `TryItOut` to expolre the data
- the data you find in the database is strucured like this:

![Entity-Relatioship-Diagram for namedentities.db](/Documentation/Report/images/er_full.png "Entity-Relatioship-Diagram for namedentities.db")


#### versions used
python v3.5.2  
spaCy v2.2.1  (model: de_core_news_md-2.2.0 (https://spacy.io/models/de))  
beautifulsoup4 v4.8.1  
sqlite3 v3.11.0  

#### If you wish to recreate one fo the steps to collect the data in the above database:
- `PersonindexScraper.py` scrapes the index of persons on Korpora.zim  
-- fills tables work, person, mention and synonym  

- `NamedEntitySearcher.py` does a full text search of the mentions in Critique of pure reason  
-- fills the table named_entity  

- `NerResultFinder.py` runs the SpaCy model on Critique of pure reason  
-- fills the tables text\_version and ner\_result    

- `FScoreCalculator.py` calculates the F-Score for the NER results  
-- fills the table f\_score  



