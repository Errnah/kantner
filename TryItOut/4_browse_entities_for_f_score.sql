/* F-Score for Critique of pure reason
#  * relevant elements: 	true named entities
#  * true positives:		ner results that match true named entities
#  * false positives:		ner results that don't match true named entities
#  * false negatives:		true named entities that don't match ner results */

/* F_SCORE */
/* browse f-score and count of all tables */
SELECT * FROM f_score

/* browse ner results and true named entities */
SELECT r.'text' as ner_result_text, s.synonym as true_named_entity, r.line as line
FROM ner_result as r
LEFT JOIN named_entity ne ON r.linenumber = ne.linenumber
LEFT JOIN synonym s ON s.syn_id = ne.syn_id
WHERE r.label = 'PER'


/* RELEVANT ELEMENTS */
/* browse true named entities */
SELECT s.synonym, ne.line FROM named_entity as ne
INNER JOIN synonym s ON s.syn_id = ne.syn_id
WHERE ne.work_id = 4
ORDER BY s.synonym


/* NER RESULTS */
/* browse ner results */
SELECT * FROM ner_result
WHERE label = 'PER'


/* TRUE POSITIVES */
/* browse true positives */
SELECT r.label AS label, r.'text' AS true_positives, r.line AS line FROM ner_result r
INNER JOIN named_entity ne ON r.startcharacter = ne.startcharacter AND r.endcharacter = ne.endcharacter










