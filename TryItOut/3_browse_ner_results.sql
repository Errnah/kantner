/* browse all ner results */
SELECT * FROM ner_result

/* browse all ner results labeled as person (after postprocessing) */
SELECT * FROM ner_result WHERE label = 'PER'

/* browse all ner results labeled as location */
SELECT * FROM ner_result WHERE label = 'LOC'

/* browse all ner results labeled as organization */
SELECT * FROM ner_result WHERE label = 'ORG'

/* browse all ner results labeled as miscellaneous */
SELECT * FROM ner_result WHERE label = 'MISC'


/* count ner results per label */
SELECT
	(SELECT COUNT(*) FROM ner_result WHERE label = 'PER') AS PER,
	(SELECT COUNT(*) FROM ner_result WHERE label = 'LOC') AS LOC,
	(SELECT COUNT(*) FROM ner_result WHERE label = 'ORG') AS ORG,
	(SELECT COUNT(*) FROM ner_result WHERE label = 'MISC') AS MISC



