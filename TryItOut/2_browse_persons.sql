/* count persons < mentions < named entities in Cpr */
SELECT
	(SELECT COUNT(DISTINCT p.pers_id) FROM person p INNER JOIN mention m ON p.pers_id = m.pers_id WHERE m.work_id = 4) AS Number_of_persons,
	(SELECT COUNT(*) FROM mention m INNER JOIN person p ON p.pers_id = m.pers_id WHERE m.work_id = 4) AS Number_of_mentions,
	(SELECT COUNT(*) AS Named_Entities_Total FROM named_entity as ne WHERE ne.work_id = 4) AS Number_of_named_entities


/* browse persons and synonyms */
SELECT p.lastname as last_name, p.firstname as first_name, s.synonym as synonym FROM synonym s
INNER JOIN person p ON s.pers_id = p.pers_id

	
/* browse persons with mentions and named entities */
SELECT p.lastname AS Person,  COUNT(DISTINCT r.ment_id) AS No_of_Mentions, COUNT(DISTINCT ne.ne_id) AS No_of_Named_Entities FROM person p
INNER JOIN named_entity ne ON p.pers_id = ne.pers_id
INNER JOIN mention r ON p.pers_id = r.pers_id
WHERE r.work_id = 4
GROUP BY P.lastname ORDER BY p.lastname

