/* browse each table */
SELECT * FROM person

SELECT * FROM synonym

SELECT * FROM mention

SELECT * FROM 'work'

SELECT * FROM text_version

SELECT * FROM named_entity
	
SELECT * FROM ner_result

SELECT * FROM f_score


/* count entries in all tables */
SELECT
	(SELECT COUNT(*) FROM person) AS Number_of_persons,
	(SELECT COUNT(*) FROM synonym) AS Number_of_synonyms,
	(SELECT COUNT(*) FROM mention) AS Number_of_mentions,
	(SELECT COUNT(*) FROM named_entity as ne) AS Number_of_Named_Entities,
	(SELECT COUNT(*) FROM ner_result) AS Number_of_NER_results,
	(SELECT COUNT(*) FROM 'work') AS Number_of_works,
	(SELECT COUNT(*) FROM text_version) AS Number_of_text_versions,
	(SELECT COUNT(*) FROM f_score) AS Number_of_F_scores
