def cleanNerResultData(c):
    c.execute("""DELETE FROM ner_result
        WHERE label = 'PER' 
            AND line LIKE '%Absch.%'
            OR line LIKE '%Abth.%'
            OR line LIKE '%Hauptst.%'
            OR line LIKE '%I. Th.%'
            OR line LIKE '1. Hauptstück. Die Disciplin der reinen Vernunft. 708'
            OR line LIKE 'Methodenlehre I. Hauptstück%'""")
