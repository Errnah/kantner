class NerResult:
    def __init__(self, label=None, text=None, line=None, linenumber=None, startCharacter=None, endCharacter=None, workId=None, textId=None):
        self.label = label
        self.text = text
        self.line = line
        self.linenumber = linenumber
        self.startCharacter = startCharacter
        self.endCharacter = endCharacter
        self.workId = workId
        self.textId = textId

    def getLabel(self):
        return self.label

    def setLabel(self, newLabel):
        self.label = newLabel

    def setText(self, newText):
        self.text = newText

    def getText(self):
        return self.text

    def getLine(self):
        return self.line

    def setLine(self, newLine):
        self.line = newLine

    def getLinenumber(self):
        return self.linenumber

    def setLinenumber(self, newLine):
        self.linenumber = newLine

    def getStartCharacter(self):
        return self.startCharacter

    def setStartCharacter(self, newStartCharacter):
        self.startCharacter = newStartCharacter

    def getEndCharacter(self):
        return self.endCharacter

    def setEndCharacter(self, newEndCharacter):
        self.EndCharacter = newEndCharacter

    def getPersonId(self):
        return self.personId

    def setPersonId(self, newPersonId):
        self.personId = newPersonId

    def getWorkId(self):
        return self.workId

    def setWorkId(self, newWorkId):
        self.workId = newWorkId

    def getTextId(self):
        return self.textId

    def setTextId(self, newTextId):
        self.textId = newTextId

    def toString(self):
        print("#########################################")
        print("\nNER_RESULT:\n")
        print("Label: " + str(self.getLabel()))
        print("Text: " + str(self.getText()))
        print("Line: " + str(self.getLine()))
        print("Linenumber: " + str(self.getLinenumber()))
        print("StartChar: " + str(self.getStartCharacter()))
        print("EndChar: " + str(self.getEndCharacter()))
        print("WorkId: " + str(self.getWorkId()))
        print("TextId: " + str(self.getTextId()))
        print("\n")
