import sqlite3
from database.DBSetupHelper import openConnection, closeConnection, dropTableFScore
from DBNerResultHelper import dropTableNerResult


def crateTableTextVersion(c):
    c.execute("""CREATE TABLE text_version(
        text_id INTEGER PRIMARY KEY NOT NULL,
        url TEXT NOT NULL,
        urn TEXT,
        title TEXT,
        year INTEGER NOT NULL,
        location TEXT NOT NULL,
        edition INTEGER NOT NULL,
        work_id INTEGER NOT NULL,
            FOREIGN KEY (work_id) REFERENCES work(work_id));""")


def fillTableTextVersion(c, url, urn, title, year, location, edition, workId):
    textVersionInsert = url, urn, title, year, location, edition, workId
    c.execute(
        """INSERT INTO text_version (url, urn, title, year, location, edition, work_id) VALUES (?, ?, ?, ?, ?, ?, ?);""", textVersionInsert)


def printTableTextVersion(c):
    c.execute("SELECT * FROM text_version;")
    res = c.fetchall()
    for r in res:
        print(r)
    c.execute("SELECT COUNT(*) FROM text_version;")
    print('TextVersions: ' + str(c.fetchone()[0]))


def dropTableTextVersion(c):
    c.execute("""DROP TABLE IF EXISTS text_version;""")


def getTextIdByUrn(c, urn):
    c.execute("""SELECT text_id FROM text_version WHERE urn = ?;""", (urn,))
    return c.fetchone()[0]


# cerate and fill table text_version with metadata
def createTextVersion(c, url, urn, title, year, location, edition, workId):
    dropTableNerResult(c)
    dropTableFScore(c)
    dropTableTextVersion(c)
    crateTableTextVersion(c)
    fillTableTextVersion(c, url, urn, title, year, location, edition, workId)
    textId = getTextIdByUrn(c, urn)
    return textId
