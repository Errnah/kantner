
def crateTableNerResult(c):
    c.execute("""CREATE TABLE ner_result(
        res_id INTEGER PRIMARY KEY NOT NULL,
        label TEXT NOT NULL,
        text TEXT NOT NULL,
        line TEXT NOT NULL,
        linenumber INTEGER NOT NULL,
        startcharacter INTEGER NOT NULL,
        endcharacter INTEGER NOT NULL,
        work_id INTEGER NOT NULL,
        text_id INTEGER NOT NULL,
            FOREIGN KEY (work_id) REFERENCES work(work_id),
            FOREIGN KEY (text_id) REFERENCES text_version(text_id));""")


def fillTableNerResult(c, nerResultList):
    for r in nerResultList:
        nerResultInsert = r.getLabel(), r.getText(), r.getLine(), r.getLinenumber(), r.getStartCharacter(
        ), r.getEndCharacter(), r.getWorkId(), r.getTextId()
        c.execute(
            """INSERT INTO ner_result (label, text, line, linenumber, startcharacter, endcharacter, work_id, text_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?);""", nerResultInsert)


def countAllNerResults(c):
    c.execute("""SELECT COUNT(*)
        FROM ner_result""")
    return c.fetchone()[0]


def fetchAllNerResults(c):
    c.execute("""SELECT *
        FROM ner_result""")
    return c.fetchall()


def printTableNerResult(c):
    c.execute("SELECT * FROM ner_result;")
    res = c.fetchall()
    for r in res:
        print(r)
    c.execute("SELECT COUNT(*) FROM ner_result;")
    print('NerResults: ' + str(c.fetchone()))


def dropTableNerResult(c):
    c.execute("""DROP TABLE IF EXISTS ner_result;""")
