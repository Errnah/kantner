import de_core_news_md
from spacy.lang.de.examples import sentences
from database.DBSetupHelper import openConnection, closeConnection
from DBNerResultHelper import fillTableNerResult, crateTableNerResult, dropTableNerResult, printTableNerResult
from NerResult import NerResult
from DBTextVersionCreator import createTextVersion, getTextIdByUrn, printTableTextVersion
from DBNerResultDataCleaner import cleanNerResultData

# choose file to process as textfilepath
krv_ocr = "../Data/kritik_der_reinen_vernunft_1781_OCR.txt"
krv_corrected = "../Data/kritik_der_reinen_vernunft_1781_corrected_preprocessed.txt"
textfilepath = krv_corrected

# fill in the metadata of the text
url = 'http://www.deutschestextarchiv.de/book/show/kant_rvernunft_1781'
urn = 'urn:nbn:de:kobv:b4-200905192998'
title = 'Critik der reinen Vernunft'
year = 1781
location = 'Riga'
edition = 1
workId = 4

# cerate and fill table text_version with metadata
conn = openConnection()
c = conn.cursor()
createTextVersion(c, url, urn, title, year, location, edition, workId)
textId = getTextIdByUrn(c, urn)


# load model and increase memory
nlp = de_core_news_md.load()
nlp.max_length = 1330000

# open file
textfile = open(textfilepath, "r")
doc = nlp(textfile.read())

NerResultList = []

# iterate through ner-output entities
for ent in doc.ents:
    # sort out lowercase results (all named entities in german should start with uppercase)
    if(not str(ent.text)[0].islower()):
        # set label
        if(ent.label_ == "LOC"):
            label = ent.label_
        if(ent.label_ == "PER"):
            label = ent.label_
        if(ent.label_ == "ORG"):
            label = ent.label_
        if(ent.label_ == "MISC"):
            label = ent.label_

        # set line and linenumber
        line = ""
        linenumber = 0
        with open(textfilepath, 'r') as file:
            characterCount = 0
            lineCount = 0
            for newLine in file:
                lineCount = lineCount + 1
                if ent.start_char >= characterCount and ent.end_char <= (characterCount + len(newLine)):
                    line = newLine.strip()
                    linenumber = lineCount
                characterCount = characterCount + len(newLine)

        # create and append entity
        newNerResult = NerResult(label, str(ent.text).strip(),
                                 line, linenumber, ent.start_char, ent.end_char, workId, textId)
        NerResultList.append(newNerResult)

# PERSIST DATA
dropTableNerResult(c)
crateTableNerResult(c)
fillTableNerResult(c, NerResultList)
cleanNerResultData(c)
printTableNerResult(c)

closeConnection(conn)
