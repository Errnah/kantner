import re

sourcefilepath = "../DataDTA/Aufklaerung/kant_aufklaerung_1784.txt"
targetfilepath = "../DataDTA/Aufklaerung/kant_aufklaerung_1784_no_linebreaks_just_a_list_of_words.txt"

sourcefile = open(sourcefilepath, "r")
targetfile = open(targetfilepath, "w")

firstpart = ""
secondpart = ""
inthemiddle = False
pattern = r'\[\d+\]|\[\d+/\d+\]'  # select numbers in braces

for line in sourcefile:
    for word in line.split():
        if not re.match(pattern, word):  # sort out pagenumbers in braces
            if inthemiddle:
                secondpart = word
                # print("2 " + secondpart)
                word = firstpart + secondpart
                if "¬" not in word:
                    # print("1+2 " + word)
                    inthemiddle = False
            if "¬" in word:
                firstpart = word
                firstpart = firstpart[:-1]
                # print("1 " + firstpart)
                inthemiddle = True
            if not inthemiddle:
                # print(word)
                targetfile.write(word + " ")

sourcefile.close()
targetfile.close()
