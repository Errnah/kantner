import sqlite3
from database.DBSetupHelper import openConnection, closeConnection
from DBFScoreHelper import countNamedEntitiesByWorkId, countNerResultsByLabel, countNamedEntitiesInNerResults, crateTableFScore, fillTableFScore, printTableFScore, dropTableFScore

#  * relevant elements: 	all named entities
#  * true positives:		named entities in results
#  * false positives:		all results - named entities in results
#  * false negatives:		all occurrences - occurrences in results

workId = 4
textId = 1

# open db connection
conn = openConnection()
c = conn.cursor()

# fetch data
personNerResults = countNerResultsByLabel(c, 'PER')
namedEntities = countNamedEntitiesByWorkId(c, workId)
truePositives = countNamedEntitiesInNerResults(c)


# calculate values
falsePositives = personNerResults - truePositives
falseNegatives = namedEntities - truePositives
precision = truePositives / personNerResults
recall = truePositives / namedEntities
fScore = 2 * ((precision * recall) / (precision + recall))


# collect values
fScoreInsert = fScore, precision, recall, truePositives, falsePositives, falseNegatives, personNerResults, namedEntities, textId


# persist values
dropTableFScore(c)
crateTableFScore(c)
fillTableFScore(c, fScoreInsert)


# close DB connection
closeConnection(conn)
