def countNamedEntitiesByWorkId(c, workId):
    c.execute("""SELECT COUNT(*)
        FROM named_entity ne
        WHERE ne.work_id = ?;""", (workId,))
    return c.fetchone()[0]


def countNerResultsByLabel(c, label):
    c.execute("""SELECT COUNT(*)
        FROM ner_result
        WHERE label = ?;""", (label,))
    return int(c.fetchone()[0])


# all named entities that have the same start- and endchar as ner results
def countNamedEntitiesInNerResults(c):
    c.execute("""SELECT COUNT(*) 
        FROM ner_result r
        INNER JOIN named_entity ne ON r.startcharacter = ne.startcharacter AND r.endcharacter = ne.endcharacter""")
    return int(c.fetchone()[0])


def crateTableFScore(c):
    c.execute("""CREATE TABLE f_score(
        fs_id INTEGER PRIMARY KEY NOT NULL,
        f_score INTEGER NOT NULL,
        precision INTEGER NOT NULL,
        recall INTEGER NOT NULL,
        true_positives INTEGER NOT NULL,
        false_positives INTEGER NOT NULL,
        false_negatives INTEGER NOT NULL,
        ner_results INTEGER NOT NULL,
        named_entities INTEGER NOT NULL,
        text_id INTEGER NOT NULL,
            FOREIGN KEY (text_id) REFERENCES text_version(text_id));""")


def fillTableFScore(c, fScoreInsert):
    c.execute("""INSERT INTO f_score (f_score, precision, recall, true_positives, false_positives, false_negatives, ner_results, named_entities, text_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);""", fScoreInsert)


def printTableFScore(c):
    c.execute("SELECT * FROM f_score;")
    res = c.fetchall()
    for r in res:
        print(r)
    c.execute("SELECT COUNT(*) FROM f_score;")
    print('F-Scores: ' + str(c.fetchone()[0]))


def dropTableFScore(c):
    c.execute("""DROP TABLE IF EXISTS f_score;""")
