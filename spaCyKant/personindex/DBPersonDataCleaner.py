from database.DBSetupHelper import openConnection, closeConnection


def insertSynonym(c, newSynonym, persId):
    c.execute("""INSERT INTO synonym(synonym, pers_id)
        VALUES(?, ?);""", (newSynonym, persId))


def getPersonIdByFullname(c, firstname, lastname):
    c.execute("""SELECT p.pers_id
        FROM person p
        WHERE p.firstname = ? AND p.lastname = ?;""", (firstname, lastname))
    return int(c.fetchone()[0])


def getPersonIdByLastname(c, lastname):
    c.execute("""SELECT p.pers_id
        FROM person p
        WHERE p.lastname = ?;""", (lastname,))
    return int(c.fetchone()[0])


def insertPersonByName(c, firstname, lastname):
    c.execute("""INSERT INTO person(firstname, lastname)
        VALUES(?, ?);""", (firstname, lastname))


def insertMention(c, workId, persId):
    c.execute("""INSERT INTO mention(work_id, pers_id)
        VALUES(?, ?);""", (workId, persId))


def updateFirstname(c, firstname, persId):
    c.execute("""UPDATE person
        SET firstname = ?
        WHERE pers_id = ?;""", (firstname, persId))


def addMissingPersons(c):
    # René Descartes has a broken firstname René/n13 618
    descartesId = getPersonIdByLastname(c, 'Descartes')
    updateFirstname(c, 'René', descartesId)

    # Descartes gets synonym Cartesius
    insertSynonym(c, 'Cartesius', descartesId)

    # Leibniz gets synonym Leibnitz
    leibnizId = getPersonIdByFullname(c, 'Gottfried Wilhelm v.', 'Leibniz')
    insertSynonym(c, 'Leibnitz', leibnizId)

    # Wolff gets synonym Wolf
    wolffId = getPersonIdByFullname(c, 'Christian', 'Wolff')
    insertSynonym(c, 'Wolf', wolffId)

    # Kant gets entry as person and synonym and mention in Kritik der reinen Vernunft (work_id = 4)
    insertPersonByName(c, 'Immanuel', 'Kant')
    kantId = getPersonIdByFullname(c, 'Immanuel', 'Kant')
    insertSynonym(c, 'Immanuel Kant', kantId)
    insertMention(c, 4, kantId)

    # David Hume gets a mention in Kritik der reinen Vernunft (work_id = 4)
    humeId = getPersonIdByFullname(c, 'David', 'Hume')
    insertMention(c, 4, humeId)

    # Hekabe gets entry as person and synonym Hekuba and mention in Kritik der reinen Vernunft (work_id = 4)
    insertPersonByName(c, None, 'Hekabe')
    hekabeId = getPersonIdByLastname(c, 'Hekabe')
    insertSynonym(c, 'Hekabe', hekabeId)
    insertSynonym(c, 'Hecuba', hekabeId)
    insertMention(c, 4, hekabeId)

    # Cicero gets a mention in Kritik der reinen Vernunft (work_id = 4)
    ciceroId = getPersonIdByLastname(c, 'Cicero')
    insertMention(c, 4, ciceroId)

    # Epikur gets synonym Epicur and mention in Kritik der reinen Vernunft (work_id = 4)
    epikurId = getPersonIdByLastname(c, 'Epikur')
    insertSynonym(c, 'Epicur', epikurId)
    insertMention(c, 4, epikurId)

    # Gott gets entry as person and synonym and mention in Kritik der reinen Vernunft (work_id = 4)
    insertPersonByName(c, None, 'Gott')
    gottId = getPersonIdByLastname(c, 'Gott')
    insertSynonym(c, 'Gott', gottId)
    insertMention(c, 4, gottId)

    # Thomas Hobbes gets a mention in Kritik der reinen Vernunft (work_id = 4)
    hobbesId = getPersonIdByFullname(c, 'Thomas', 'Hobbes')
    insertMention(c, 4, hobbesId)

    # Charles de Bonnet gets a mention in Kritik der reinen Vernunft (work_id = 4)
    bonnetId = getPersonIdByFullname(c, 'Charles de', 'Bonnet')
    insertMention(c, 4, bonnetId)

    # Johann Friedrich sen. Hartknoch gets synonym Johann Friedrich Hartknoch and mention in Kritik der reinen Vernunft (work_id = 4)
    hartknochId = getPersonIdByFullname(
        c, 'Johann Friedrich sen.', 'Hartknoch')
    insertSynonym(c, 'Johann Friedrich Hartknoch', hartknochId)
    insertMention(c, 4, hartknochId)

    # Johann Heinrich Lambert gets a mention in Kritik der reinen Vernunft (work_id = 4)
    lambertId = getPersonIdByFullname(c, 'Johann Heinrich', 'Lambert')
    insertMention(c, 4, lambertId)

    # Solon gets synonym Solones and a mention in Kritik der reinen Vernunft (work_id = 4)
    solonId = getPersonIdByLastname(c, 'Solon')
    insertSynonym(c, 'Solones', solonId)
    insertMention(c, 4, solonId)
