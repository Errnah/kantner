from database.DBSetupHelper import openConnection, closeConnection


def createTableWork(c):
    c.execute("""CREATE TABLE work(
        work_id INTEGER PRIMARY KEY NOT NULL,
        title TEXT NOT NULL,
        volume INTEGER NOT NULL,
        startpage INTEGER,
        endpage INTEGER,
        edition INTEGER NOT NULL);""")


def fillAllTablesWork(c):
    fillTableWork1(c)
    fillTableWork2(c)
    fillTableWork3(c)
    fillTableWork4(c)
    fillTableWork5(c)
    fillTableWork6(c)
    fillTableWork7(c)
    fillTableWork8(c)
    fillTableWork9(c)
    fillTableWork10(c)
    fillTableWork11(c)
    fillTableWork12(c)
    fillTableWork13(c)
    fillTableWork14(c)
    fillTableWork15(c)
    fillTableWork16(c)
    fillTableWork17(c)


def dropTableWork(c):
    c.execute("""DROP TABLE IF EXISTS work;""")


def printTableWork(c):
    c.execute("SELECT * FROM work;")
    res = c.fetchall()
    for r in res:
        print(r)
    c.execute("SELECT COUNT(*) FROM work;")
    print('Works: ' + str(c.fetchone()[0]))


def fillTableWork1(c):
    title = 'Vorkritische Schriften I'
    volume = 1
    startpage = None
    endpage = None
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork2(c):
    title = 'Vorkritische Schriften II'
    volume = 2
    startpage = None
    endpage = None
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork3(c):
    title = 'Kritik der reinen Vernunft 1787'
    volume = 3
    startpage = None
    endpage = None
    edition = 2
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork4(c):
    title = 'Kritik der reinen Vernunft 1781'
    volume = 4
    startpage = 1
    endpage = 252
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork5(c):
    title = 'Prolegomena zu einer jeden künftigen Metaphysik, die als Wissenschaft wird auftreten können'
    volume = 4
    startpage = 253
    endpage = 384
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork6(c):
    title = 'Grundlegung zur Metaphysik der Sitten'
    volume = 4
    startpage = 385
    endpage = 464
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork7(c):
    title = 'Metaphysische Anfangsgründe der Naturwissenschaft'
    volume = 4
    startpage = 465
    endpage = None
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork8(c):
    title = 'Kritik der praktischen Vernunft'
    volume = 5
    startpage = 1
    endpage = 164
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork9(c):
    title = 'Kritik der Urteilskraft'
    volume = 5
    startpage = 165
    endpage = None
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork10(c):
    title = 'Die Religion innerhalb der Grenzen der bloßen Vernunft'
    volume = 6
    startpage = 1
    endpage = 202
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork11(c):
    title = 'Die Metaphysik der Sitten'
    volume = 6
    startpage = 203
    endpage = None
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork12(c):
    title = 'Der Streit der Fakultäten'
    volume = 7
    startpage = 1
    endpage = 116
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork13(c):
    title = 'Anthropologie in pragmatischer Hinsicht'
    volume = 7
    startpage = 117
    endpage = None
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork14(c):
    title = 'Abhandlungen nach 1781'
    volume = 8
    startpage = None
    endpage = None
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork15(c):
    title = 'Logik'
    volume = 9
    startpage = None
    endpage = 150
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork16(c):
    title = 'Physische Geographie'
    volume = 9
    startpage = 151
    endpage = 436
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


def fillTableWork17(c):
    title = 'Pädagogik'
    volume = 9
    startpage = 437
    endpage = None
    edition = 1
    workInsert = title, volume, startpage, endpage, edition
    c.execute(
        """INSERT INTO work (title, volume, startpage, endpage, edition) VALUES (?, ?, ?, ?, ?);""", workInsert,)


conn = openConnection()
c = conn.cursor()

dropTableWork(c)
createTableWork(c)

fillAllTablesWork(c)
printTableWork(c)

closeConnection(conn)
