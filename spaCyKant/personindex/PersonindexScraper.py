import re
import sqlite3
from PersonindexScraperHelper import getBeautifulSoup, getBeautifulSoupForLetter, extractFirstName, extractLastName, lookupSynonym, extractInfo, printScrapedData, extractMentionAsInt, eliminateDuplicates
from DBPersonDataCleaner import addMissingPersons
from DBPersonHelper import createTableMention, createTablePerson, createTableSynonym, dropTheTables, fillTablesPersonMentionSynonym, printTableMention
from database.DBSetupHelper import openConnection, closeConnection, dropAllTables
from Person import Person

# SCRAPE
personList = []
synonymList = dict()
person = None
websiteContent = getBeautifulSoup()

# regex matcher for the references of a person in a text
# letters = r"[0-9{3}a-z(,1}]*$"  # for now i'm ignoring letters
# works = r"[1-9]{2}\s[0-9]{3}\s[0-9]{2}"  # volume, page, line (VV PPP LL)
worksPart1 = r"0[1-9]\s[0-9]{3}\s[0-9]{2}"  # volumes 1-9 only (Abt. I)

for element in websiteContent.findAll(["a", "b"]):
    # only persons names are bold
    if "<b>" in str(element):
        # save previous person
        if person is not None:
            personList.append(person)

        # name is a synonym for the name of the person where the references are
        if " s. " in element.text:
            name = element.text.split(" s. ")[1].strip()
            synonym = element.text.split(" s. ")[0].strip()

            # check existing persons for matches with synonym
            for p in personList:
                # person with synonym already exisit -> just add synonym to existing person
                if name == p.getLastName():
                    p.setSynonym(synonym)
                # person with synonym was not found yet -> add synonym to synonymList
                else:
                    synonymList[name] = synonym

        # name is no synonym
        else:
            # create new person with values from name element
            rawName = element.text
            firstName = extractFirstName(rawName)
            lastName = extractLastName(rawName)
            info = extractInfo(rawName)
            synonym = lookupSynonym(lastName, synonymList)

            person = Person(firstName, lastName, info, synonym)

    # references are numbers as text of links, matcher selects nr-format and part of text
    if "<a href" in str(element) and re.match(worksPart1, element.text):
        mention = extractMentionAsInt(element.text)
        person.addMention(mention)

# append last person as well
personList.append(person)


# CLEANUP
# delete all persons with no refernces (in Abt. I)
personList = [p for p in personList if len(p.mentions) is not 0]
# could not find source of problem so i cured the symptom, sorry
personList = eliminateDuplicates(personList)


# SQLITE PERSIST DATA
conn = openConnection()
c = conn.cursor()


# (re-) create tables
dropAllTables(c)

createTablePerson(c)
createTableMention(c)
createTableSynonym(c)


# fill tables
fillTablesPersonMentionSynonym(c, personList)
addMissingPersons(c)

# close DB connection
closeConnection(conn)

# THE RAW DATA
# <tr>
# <td width="3%"> </td>
# <td colspan="4"><b> name, vorname der person </b>(lebensdaten)</td>
# <td>stelle mit erlaeuterungen zur person</td> z.b. 13 603 -> Band 13, Seite 603
# <td width="3%"> </td>
# </tr>
# <tr>
# <td width="3%"> </td>
# <td><a href="../aa02/283.html#z33">stelle mit nennung der person</a></td> 10 488 36 -> Band 10, Seite 488, Zeile 36
# <td><a href="../aa08/361.html#z11">stelle mit nennung der person</a></td> 324 oder 483a -> Brief (vllt erstmal ignorieren)
# <td><a href="../aa12/163.html#z31">stelle mit nennung der person</a></td>
# <td><a href="../aa20/072.html#z14">stelle mit nennung der person</a></td>
# <td><a href="../aa22/154.html#z28">stelle mit nennung der person</a></td>
# <td width="3%"> </td>
# </tr>
