import string
import requests
import bs4


# BEAUTIFULSOUP

# gets a Beautifulsoup object for all subsites (register has one for each letter in the alphabet)
def getBeautifulSoup():
    urls = []
    website = ""
    # get URLs for all subsites
    for letter in list(string.ascii_uppercase):
        url = "https://korpora.zim.uni-duisburg-essen.de/Kant/persindex/PI-" + letter + ".html"
        urls.append(url)
    # get-request to all subsites
    for url in urls:
        website += requests.get(url).text
    # returns a Beautifulsoup object of all subsites
    return bs4.BeautifulSoup(website, 'html.parser')


# gets a Beautifulsoup object for the subsite with given letter
def getBeautifulSoupForLetter(letter):
    url = "https://korpora.zim.uni-duisburg-essen.de/Kant/persindex/PI-" + letter + ".html"
    website = requests.get(url).text
    return bs4.BeautifulSoup(website, 'html.parser')


# PERSON DATA

def extractLastName(rawName):
    lastName = None
    fullName = rawName.split(" [")[0]
    if "," in fullName:
        lastName = fullName.split(",")[0].strip()
    else:
        lastName = fullName.strip()
    return lastName


def extractFirstName(rawName):
    firstName = None
    fullName = rawName.split(" [")[0]
    if "," in fullName:
        firstName = fullName.split(",")[1].strip()
    return firstName


def extractInfo(rawName):
    info = None
    if "[" in rawName:
        rawInfo = rawName.split("[")[1]
        info = rawInfo.split("]")[0].strip()
    return info


def extractMentionAsInt(rawMention):
    splitMention = rawMention.strip().split(" ")
    mention = int(splitMention[0].strip()), int(
        splitMention[1].strip()), int(splitMention[2].strip())
    return mention


# if lastName is in synonymList set synonym, else None
def lookupSynonym(lastName, synonymList):
    synonym = None
    if lastName in synonymList:
        synonym = synonymList[lastName]
    return synonym


def eliminateDuplicates(personList):
    for p in personList:
        f = p.getFirstName()
        l = p.getLastName()
        i = p.getInfo()
        s = p.getSynonym()
        count = 0
        for pp in personList:
            if f == pp.getFirstName() and l == pp.getLastName() and i == pp.getInfo() and s == pp.getSynonym():
                count = count + 1
                if count > 1:
                    personList.remove(p)
    return personList


def printScrapedData(personList):
    syn = 0
    inf = 0
    men = 0

    for p in personList:
        print("\n+++++PERSON+++++\n")
        print(p.lastName)
        print(p.firstName)
        print(p.synonym)
        if p.synonym:
            syn = syn+1
        print(p.info)
        if p.info:
            inf = inf+1
        print(p.mentions)
        men += len(p.mentions)

    print("\n*****TOTALS*****\n")
    print("pers: " + str(len(personList)))
    print("syno: " + str(syn))
    print("info: " + str(inf))
    print("ment: " + str(men))
