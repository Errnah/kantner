class Person:

    def __init__(self, firstName=None, lastName=None, synonym=None,  info=None, refs=[], id=None):
        self.firstName = firstName
        self.lastName = lastName
        self.synonym = synonym
        self.info = info
        self.mentions = []
        self.id = id

    def getId(self):
        return self.id

    def getFirstName(self):
        return self.firstName

    def setFirstName(self, newFirstName):
        self.firstName = newFirstName

    def getLastName(self):
        return self.lastName

    def setLastName(self, newLastName):
        self.lastName = newLastName

    def getSynonym(self):
        return self.synonym

    def setSynonym(self, newSynonym):
        self.synonym = newSynonym

    def getInfo(self):
        return self.info

    def setInfo(self, newInfo):
        self.info = newInfo

    def getMentions(self):
        return self.mentions

    def addMention(self, newMention):
        self.mentions.append(newMention)

    def toString(self):
        print("#########################################")
        print("\nPERSON:\n")
        print("Id: " + str(self.getId()))
        print("FirstName: " + str(self.getFirstName()))
        print("LastName: " + str(self.getLastName()))
        print("Synonym: " + str(self.getSynonym()))
        print("Info: " + str(self.getInfo()))
        print("Mentions: ")
        for m in self.getMentions():
            print(str(m))
        print("\n")
