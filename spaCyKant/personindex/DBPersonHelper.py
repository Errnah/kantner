import sqlite3
from database.DBSetupHelper import openConnection, closeConnection, dropAllTables
from Person import Person


# CREATE TABLES
def createTablePerson(c):
    c.execute("""CREATE TABLE person(
        pers_id INTEGER PRIMARY KEY NOT NULL,
        firstname TEXT,
        lastname TEXT NOT NULL);""")


def createTableSynonym(c):
    c.execute("""CREATE TABLE synonym(
        syn_id INTEGER PRIMARY KEY NOT NULL,
        synonym TEXT NOT NULL,
        pers_id INTEGER NOT NULL,
            FOREIGN KEY (pers_id) REFERENCES person(pers_id));""")


def createTableMention(c):
    c.execute("""CREATE TABLE mention(
        ment_id INTEGER PRIMARY KEY NOT NULL,
        pagenumber INTEGER,
        linenumber INTEGER,
        pers_id INTEGER NOT NULL,
        work_id INTEGER NOT NULL,
            FOREIGN KEY (pers_id) REFERENCES person(pers_id),
            FOREIGN KEY (work_id) REFERENCES work(work_id));""")


# FILL TABLES
def fillTablesPersonMentionSynonym(c, scrapedPersonList):
    for p in scrapedPersonList:
        # fill table person
        personInsert = p.getFirstName(), p.getLastName()
        c.execute(
            """INSERT INTO person (firstname, lastname) VALUES (?, ?);""", personInsert,)
        pers_id = c.lastrowid

        # fill table mention with data from personList
        for mention in p.mentions:
            volume = mention[0]
            pagenumber = mention[1]
            linenumber = mention[2]
            # set work_id
            work_id = None
            if(volume == 1):
                work_id = 1
            if(volume == 2):
                work_id = 2
            if(volume == 3):
                work_id = 3
            if(volume == 4 and pagenumber <= 252):
                work_id = 4
            if(volume == 4 and pagenumber >= 253 and pagenumber <= 384):
                work_id = 5
            if(volume == 4 and pagenumber >= 385 and pagenumber <= 464):
                work_id = 6
            if(volume == 4 and pagenumber >= 465):
                work_id = 7
            if(volume == 5 and pagenumber <= 164):
                work_id = 8
            if(volume == 5 and pagenumber >= 165):
                work_id = 9
            if(volume == 6 and pagenumber <= 202):
                work_id = 10
            if(volume == 6 and pagenumber >= 203):
                work_id = 11
            if(volume == 7 and pagenumber <= 116):
                work_id = 12
            if(volume == 7 and pagenumber >= 117):
                work_id = 13
            if(volume == 8):
                work_id = 14
            if(volume == 9 and pagenumber <= 150):
                work_id = 15
            if(volume == 9 and pagenumber >= 151 and pagenumber <= 436):
                work_id = 16
            if(volume == 9 and pagenumber >= 437):
                work_id = 17

            mentionInsert = pagenumber, linenumber, pers_id, work_id
            c.executemany(
                """INSERT INTO mention (pagenumber, linenumber, pers_id, work_id) VALUES (?, ?, ?, ?);""", (mentionInsert,))

        # fill table synonym with data from personList
        if p.getLastName():
            synInsertLast = p.getLastName(), pers_id
            c.executemany(
                """INSERT INTO synonym (synonym, pers_id) VALUES (?, ?);""", (synInsertLast,))

        if p.getFirstName():
            firstname = str(p.getFirstName())
            fullName = firstname + " " + str(p.getLastName())
            synInsertFull = fullName, pers_id
            c.executemany(
                """INSERT INTO synonym (synonym, pers_id) VALUES (?, ?);""", (synInsertFull,))

            if ' ' in firstname:
                synonyms = firstname.split()
                for s in synonyms:
                    if(not str(s)[0].islower()):
                        synInsert = s, pers_id
                        c.executemany(
                            """INSERT INTO synonym (synonym, pers_id) VALUES (?, ?);""", (synInsert,))


# DROP TABLES
def dropTheTables(c):
    dropTableSynonym(c)
    dropTableMention(c)
    dropTablePerson(c)  # person must be dropped last because of foreign keys


def dropTablePerson(c):
    c.execute("""DROP TABLE IF EXISTS person;""")


def dropTableSynonym(c):
    c.execute("""DROP TABLE IF EXISTS synonym;""")


def dropTableMention(c):
    c.execute("""DROP TABLE IF EXISTS mention;""")


# FETCH TABLES
# Kritik der reinen Vernunft (AA: vol 4, p 1-252)
def fetchAllPersonsInKritikDerReinenVernunft(c):
    c.execute("""SELECT p.firstname, p.lastname, p.synonym, p.info, p.pers_id
        FROM mention m
        INNER JOIN person p ON p.pers_id = m.pers_id
        WHERE m.work_id = 4
        GROUP BY p.pers_id ORDER BY p.lastname""")
    persons = c.fetchall()
    personList = []
    for p in persons:
        pers = Person(p[0], p[1], p[2], p[3], [], p[4])
        personList.append(pers)
    return personList


# Kritik der praktischen Vernunft (AA: vol 5, p 1-164)
def fetchAllPersonsInKritikDerPraktischenVernunft(c):
    c.execute("""SELECT p.firstname, p.lastname, p.synonym, p.info, p.pers_id 
        FROM mention m
        INNER JOIN person p ON p.pers_id = m.pers_id
        WHERE m.work_id = 4
        GROUP BY p.pers_id ORDER BY p.lastname""")
    persons = c.fetchall()
    personList = []
    for p in persons:
        pers = Person(p[0], p[1], p[2], p[3], [], p[4])
        personList.append(pers)
    return personList


def fetchAllPersons(c):
    c.execute("""SELECT p.lastname, p.pers_id
        FROM mention m
        INNER JOIN person p ON p.pers_id = m.pers_id
        GROUP BY p.pers_id ORDER BY p.lastname""")
    return c.fetchall()


# PRINT TABLES
def printAllTables(c):
    printTablePerson(c)
    printTableMention(c)
    printTableSynonym(c)


def printTablePerson(c):
    c.execute("SELECT * FROM person;")
    res = c.fetchall()
    for r in res:
        print(r)
    c.execute("SELECT COUNT(*) FROM person;")
    print('Persons: ' + str(c.fetchone()[0]))


def printTableMention(c):
    c.execute("SELECT * FROM mention;")
    res = c.fetchall()
    for r in res:
        print(r)
    c.execute("SELECT COUNT(*) FROM mention;")
    print('Mentions: ' + str(c.fetchone()[0]))


def printTableSynonym(c):
    c.execute("SELECT * FROM synonym;")
    res = c.fetchall()
    for r in res:
        print(r)
    c.execute("SELECT COUNT(*) FROM synonym;")
    print('Synonyms: ' + str(c.fetchone()[0]))
