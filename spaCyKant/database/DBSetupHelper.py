import sqlite3


# ImportError: No module named 'database' -> export PYTHONPATH="$PYTHONPATH:/home/erna/Projects/kantner/kantner/spaCyKant"


# OPEN DB CONNECTION
def openConnection():
    # creates DB in ram, a fresh DB for every run
    # conn = sqlite3.connect(':memory:')
    # conn.execute("PRAGMA foreign_keys = ON")

    # creates DB in file, will be persisted in the file *.db
    conn = sqlite3.connect('database/namedentities.db')
    conn.execute("PRAGMA foreign_keys = ON;")
    return conn


# CLOSE DB CONNECTION
def closeConnection(conn):
    conn.commit()
    conn.close()


# DROP TABLES
def dropAllTables(c):
    dropTableNamedEntity(c)
    dropTableSynonym(c)
    dropTableMention(c)
    dropTablePerson(c)  # person must be dropped last because of foreign keys


def dropTablePerson(c):
    c.execute("""DROP TABLE IF EXISTS person;""")


def dropTableMention(c):
    c.execute("""DROP TABLE IF EXISTS mention;""")


def dropTableNamedEntity(c):
    c.execute("""DROP TABLE IF EXISTS named_entity;""")


def dropTableSynonym(c):
    c.execute("""DROP TABLE IF EXISTS synonym;""")


def dropTableFScore(c):
    c.execute("""DROP TABLE IF EXISTS f_score;""")
