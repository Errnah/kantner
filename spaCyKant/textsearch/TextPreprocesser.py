# replace 'ſ' with 's' and 'aͤ' with 'ä', etc. for all umlauts
def preprocess(inputfile, outputfile):
    with open(inputfile, 'r') as originalfile:
        with open(outputfile, 'w') as searchfile:
            for line in originalfile:
                newline = line.replace('ſ', 's').replace(
                    'aͤ', 'ä').replace('uͤ', 'ü').replace('oͤ', 'ö')
                # print(newline)
                searchfile.write(newline)
    return outputfile


krv_corrected = "../Data/kritik_der_reinen_vernunft_1781_corrected.txt"
krv_corrected_preprocessed = "../Data/kritik_der_reinen_vernunft_1781_corrected_preprocessed.txt"


krv_corrected_preprocessed = preprocess(
    krv_corrected, krv_corrected_preprocessed)
