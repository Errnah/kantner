import sqlite3
from database.DBSetupHelper import openConnection, closeConnection
from DBNamedEntityHelper import crateTableNamedEntity, fillTableNamedEntity, dropTableNamedEntity, fetchAllSnonymsInWork, fetchAllSnonyms, printTableNamedEntity
from NamedEntitySearcherHelper import calcFirstChar, printStatistic
from NamedEntity import NamedEntity

# file to get the occurrences from
krv_corrected = "../Data/kritik_der_reinen_vernunft_1781_corrected_preprocessed.txt"
# set name of work
workTitle = "Kritik der reinen Vernunft 1781"
workId = 4

# open db connection and get personList
conn = openConnection()
c = conn.cursor()
synonymList = fetchAllSnonymsInWork(c, workId)

namedEntityList = []


with open(krv_corrected, 'r') as file:
    linenumber = 0
    characterCount = 0
    for line in file:
        linenumber = linenumber + 1
        for s in synonymList:
            searchterm = str(s.getSynonym())
            if searchterm in line:
                startChar = calcFirstChar(line, searchterm, characterCount)
                endChar = startChar + len(searchterm)
                namedEntity = NamedEntity(
                    line.strip(), linenumber, startChar, endChar, s.getPersonId(),  s.getSynonymId(), workId)
                namedEntityList.append(namedEntity)
        characterCount = characterCount + len(line)


# SQLITE PERSIST DATA
dropTableNamedEntity(c)
crateTableNamedEntity(c)
fillTableNamedEntity(c, namedEntityList, synonymList)
printTableNamedEntity(c)

closeConnection(conn)

# PRINT FILE STATS
# printStatistic(krv_corrected)
