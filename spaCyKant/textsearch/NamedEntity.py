class NamedEntity:

    def __init__(self, line=None, linenumber=None, startCharacter=None, endCharacter=None, personId=None, synonymId=None, workId=None):
        self.line = line
        self.linenumber = linenumber
        self.startCharacter = startCharacter
        self.endCharacter = endCharacter
        self.personId = personId
        self.synonymId = synonymId
        self.workId = workId

    def getLine(self):
        return self.line

    def setLine(self, newLine):
        self.line = newLine

    def getLinenumber(self):
        return self.linenumber

    def setLinenumber(self, newLinenumber):
        self.linenumber = newLinenumber

    def getStartCharacter(self):
        return self.startCharacter

    def setStartCharacter(self, newStartCharacter):
        self.startCharacter = newStartCharacter

    def getEndCharacter(self):
        return self.endCharacter

    def setEndCharacter(self, newEndCharacter):
        self.EndCharacter = newEndCharacter

    def getPersonId(self):
        return self.personId

    def setPersonId(self, newPersonId):
        self.personId = newPersonId

    def getSynonymId(self):
        return self.synonymId

    def setSynonymId(self, newSynonymId):
        self.SynonymId = newSynonymId

    def getWorkId(self):
        return self.workId

    def setWorkId(self, newWorkId):
        self.workId = newWorkId

    def toString(self):
        print("#########################################")
        print("\nNAMED_ENTITY:\n")
        print("Line: " + str(self.getLine()))
        print("Linenumber: " + str(self.getLinenumber()))
        print("StartChar: " + str(self.getStartCharacter()))
        print("EndChar: " + str(self.getEndCharacter()))
        print("PersonId: " + str(self.getPersonId()))
        print("SynonymId: " + str(self.getSynonymId()))
        print("WorkId: " + str(self.getWorkId()))
        print("\n")
