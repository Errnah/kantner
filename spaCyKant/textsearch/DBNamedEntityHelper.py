import sqlite3
from NamedEntity import NamedEntity
from Synonym import Synonym


# CREATE TABLES
def crateTableNamedEntity(c):
    c.execute("""CREATE TABLE named_entity(
        ne_id INTEGER PRIMARY KEY NOT NULL,
        line TEXT NOT NULL,
        linenumber INTEGER NOT NULL,
        startcharacter INTEGER NOT NULL,
        endcharacter INTEGER NOT NULL,
        pers_id INTEGER NOT NULL,
        syn_id INTEGER NOT NULL,
        work_id INTEGER NOT NULL,
            FOREIGN KEY (pers_id) REFERENCES person(pers_id),
            FOREIGN KEY (syn_id) REFERENCES synonym(syn_id),
            FOREIGN KEY (work_id) REFERENCES work(work_id));""")


# FILL TABLES
def fillTableNamedEntity(c, namedEntityList, synonymList):
    for ne in namedEntityList:
        pers_id = ne.getPersonId()
        for s in synonymList:
            if pers_id == s.getPersonId():
                namedEntityInsert = ne.getLine(), ne.getLinenumber(), ne.getStartCharacter(
                ), ne.getEndCharacter(), ne.getSynonymId(), pers_id, ne.getWorkId()
                c.execute(
                    """INSERT INTO named_entity (line, linenumber, startcharacter, endcharacter, syn_id, pers_id, work_id) VALUES (?, ?, ?, ?, ?, ?, ?);""", namedEntityInsert)


# DROP TABLES
def dropAllTables(c):
    dropTableNamedEntity(c)
    dropTableSynonym(c)


def dropTableSynonym(c):
    c.execute("""DROP TABLE IF EXISTS synonym;""")


def dropTableNamedEntity(c):
    c.execute("""DROP TABLE IF EXISTS named_entity;""")


# FETCH TABLES

def fetchAllNamedEntitiesInWork(c, workId):
    c.execute("""SELECT *
        FROM named_entity ne
        WHERE ne.work_id = ?""", (workId))
    return c.fetchall()


def fetchAllPersonsWithNamedEntitiesCountInKritikDerReinenVernunft(c):
    c.execute("""SELECT P.lastname AS Person, COUNT(*) AS No_of_Named_Entities 
        FROM named_entity ne
        INNER JOIN person p ON p.pers_id = ne.pers_id
        GROUP BY P.lastname ORDER BY p.lastname""")
    return c.fetchall()


# Kritik der reinen Vernunft (AA: vol 4, p 1-252)
def fetchAllSnonymsInWork(c, workId):
    c.execute("""SELECT s.syn_id, s.synonym, s.pers_id
        FROM synonym s
        INNER JOIN person p ON p.pers_id = s.pers_id
        INNER JOIN mention m ON p.pers_id = m.pers_id
        WHERE m.work_id = ?
        GROUP BY p.pers_id ORDER BY p.lastname""", (workId,))
    synonyms = c.fetchall()
    synonymList = []
    for s in synonyms:
        syn = Synonym(s[0], s[1], s[2])
        synonymList.append(syn)
    return synonymList


def fetchAllSnonyms(c):
    c.execute("""SELECT s.syn_id, s.synonym, s.pers_id
        FROM synonym s
        INNER JOIN person p ON p.pers_id = s.pers_id
        GROUP BY p.pers_id ORDER BY p.lastname""")
    synonyms = c.fetchall()
    synonymList = []
    for s in synonyms:
        syn = Synonym(s[0], s[1], s[2])
        synonymList.append(syn)
    return synonymList


# PRINT TABLES
def printAllTables(c):
    printTableSynonym(c)
    printTableNamedEntity(c)


def printTableSynonym(c):
    c.execute("SELECT * FROM synonym;")
    res = c.fetchall()
    for r in res:
        print(r)
    c.execute("SELECT COUNT(*) FROM synonym;")
    print('Synonyms: ' + str(c.fetchone()))


def printTableNamedEntity(c):
    c.execute("SELECT * FROM named_entity;")
    res = c.fetchall()
    for r in res:
        print(r)
    c.execute("SELECT COUNT(*) FROM named_entity;")
    print('NamedEntities: ' + str(c.fetchone()))
