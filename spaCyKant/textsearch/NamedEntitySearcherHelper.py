def calcFirstChar(line, searchTerm, characterCount):
    parts = line.split(searchTerm)
    countFirstPart = len(parts[0])
    firstChar = characterCount + countFirstPart
    return firstChar


def printStatistic(file):
    characterCount = 0
    print("\n" + "++++++++++STATISTICS OF THE SEARCHED TEXT+++++++++++" + "\n")
    lines = 0
    words = 0
    with open(file) as file:
        for line in file:
            wordslist = line.split()
            lines = lines + 1
            words = words + len(wordslist)
            characterCount = characterCount + len(line)
    print("File: " + file.name)
    print("Lines: " + str(lines))
    print("Words: " + str(words))
    print("Characters: " + str(characterCount))
    print("\n")
