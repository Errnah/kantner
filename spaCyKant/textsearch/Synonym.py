class Synonym:
    def __init__(self, synonymId, synonym, personId=None):
        self.synonymId = synonymId
        self.synonym = synonym
        self.personId = personId

    def getSynonymId(self):
        return self.synonymId

    def getSynonym(self):
        return self.synonym

    def setSynonym(self, newSynonym):
        self.synonym = newSynonym

    def getPersonId(self):
        return self.personId

    def setPersonId(self, newPersonId):
        self.personId = newPersonId

    def toString(self):
        print("#########################################")
        print("\nSYNONYM:\n")
        print("Synonym: " + str(self.getSynonym()))
        print("PersonId: " + str(self.getPersonId()))
        print("\n")
