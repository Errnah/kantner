\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}


\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{float}
\usepackage{lmodern}

% Textformatierung
\usepackage{tabto}
\usepackage[onehalfspacing]{setspace}
\fontfamily{times}
\usepackage[ampersand]{easylist}
%\ListProperties(Hang=true, Progressive=3ex)
\usepackage{verbatim}

% Seitenlayout
\usepackage[
    a4paper,
    layoutvoffset=0cm,
    margin=0cm,
    centering,
    left=3.0cm, %linker Rand: 3,0cm    
    right=2.5cm, %rechter Rand: 2,5cm    
    top=2.5cm, %oberer Rand: 2,5cm    
    bottom=1.5cm, %unterer Rand: 1,5cm    
    includeheadfoot %Kopf- und Fußzeile
]{geometry}
\savegeometry{BA_geometry}

% Biblatex
\usepackage{csquotes}
\usepackage{xpatch}
%\usepackage[linktocpage=true]{hyperref}
\usepackage[
    backend=bibtex,
    natbib=true,
    bibencoding=ascii,
    style=alphabetic,
    bibstyle=alphabetic,    %in Verzeichnis
    %citestyle=authoryear,    %im Text
    %sorting=nyt,
    maxcitenames=2,
    %giveninits=true,
    uniquename=false,
    uniquelist=false,
    %dashed=false, % prints repeating authorname instead of a dash
    maxbibnames=99, % prints all authors in bibliography
    doi=false,
    isbn=false,
    url=false,
    %stuff for alphabetic
    maxalphanames=1,
    %indexing=bib,    
]{biblatex}

\bibliography{KantNER_Report_Biblography}


\begin{document}

%Deckblatt
\thispagestyle{empty}
\include{KantNER_Report_Cover}

% Inhaltsverzeichnis
\tableofcontents
\addcontentsline{toc}{section}{Table of Content}
\pagenumbering{roman}
\newpage

% Einleitung
\pagenumbering{gobble}
\pagestyle{plain}
\pagenumbering{arabic}
\setcounter{page}{1}

\section{Introduction} \label{idea}
Printed editions of the works of Immanuel Kant have a long tradition. Over hundreds of years the texts were edited and published a multitude of times. Those editions often contain indexes for persons mentioned in the text. Since the digitalization of texts there are some online editions too \citep{bbaw2019index}
\citep{korpora2019index}, \citep{archive2019index}. But they mostly just provide the text. One advantage in comparison with printed editions is that full text search can be done. In one case even a digitalized index of persons is available \citep{korpora2019persindex}. It provides hyperlinks from the index of persons to the line in the text where the person is mentioned. That is already very useful. But what about the other way around? A smart text online edition could provide additional information about that person mentioned in the text e.g. when the reader moves the cursor over the person's name. The field of \textit{Digital Humanities} (DH) aims to explore the application of computer science to questions of humanities. Without compromising the beauty of reading a printed book on a sunny day in the park, such tools could aid the research in philosophy or literature. To achieve that a complete list of all occurrences of every person mentioned in the text is needed. One way to create such a list would be to manually go through the text. But that would be a tedious and time consuming task for a human. Can an index of persons be created automatically with the help of computer science? The aim of the presented project KantNER is to contribute to the answer to the above question and create a possible tool chain to retrieve the data needed for a smart text online edition using the example of persons appearing in the 'Critique of pure reason' (Cpr) as one of the most famous works of Immanuel Kant.
The technique of \textit{named entity recognition} (NER) uses neural networks to identify possible named entities in a text. To rate the performance of a NER model the F-Score is a broadly used tool. It gives a measure of the goodness of the results that is independent of the length of the text or the frequency of wanted results in the text to be searched. The goal of KantNER  is to first create an index of persons that is assumed to be complete, second run an NER model and third to compare both results to rate the quality of the NER model applied to the Cpr. This setup allows the 16th century German of Immanuel Kant to be a touchstone for NER models. It could also be a step towards creating a smart text edition. Since this is considered a pre-study the results will be in the form of a database whose creation and final data will be explained in this report. 

\section{Text and Model} \label{a text and model}
In this chapter the choice of the text version as well as the NER model are described, as both will impact the results of KantNER  described above. The version of the text will influence the ability to create a list of persons that will be used to as test data for the NER output. Several digitalized versions of Cpr are currently available online, but they differ in quality. Some are expensively corrected by hand, some are the raw output of \textit{optical character recognition} (OCR) methods. Among many, I chose the first edition of Cpr printed 1781 by Hartknoch in Riga. In order to create a list of true named entities a high quality version of the text is needed because a full text search will be performed to find all occurrences. The best quality edition I found was published by Deutsches Text Archiv (DTA) \citep{dta2019krv}. It is corrected according to the guidelines for transcription by DTA \citep{dta2019trans}. The only preprocessing was to replace umlauts and the lower case letter s with modern characters.

The choice of the NER model is first limited through the supported languages, as the text to process is written in German. It's performance will influence the degree of automation \textit{versus} manual effort that will be necessary to create a smart text edition. SpaCy is a Python library for natural language processing that provides pre-trained models for named entity recognition in german texts \citep{spacy2019index}. 
The model \textit{de\_core\_news\_md-2.2.0}
is a \textit{Convolutional Neural Network} (CNN) that was trained on the TIGER \citep{tiger2019index} and WikiNER \citep{nothman2013learning} corpus. It classifies named entities as either a person, a location, an organization or miscellaneous \citep{spacy2019models}. Based on the annotations in the WikiNER corpus the model reaches an F-Score of 0.8342, Precision of 0.8378 and a Recall of 0.8307. The F-Score ranges between one and 100. For models trained for languages other than English as the current \textit{lingua franca} these scores mirror the abilities of NER models today. Since the model was trained on Wikipedia that contains todays German in the form of lexicon articles the results on the works of Kant are expected to be lower due to different time of creation and genre of the two text sources. Named Entities are detected based on their Embedding Spaces, i.e. a few words before and after the supposed named entity. If the words around a term are similar to words that often surround a confirmed named entity from the training data the CNN considers the term a named entity. Kant's choice of vocabulary as well as his extensive usage of nested and chained sentences can be assumed very different than the style of language used in Wikipedia articles. However with a good quality text and a model with a F-Score above 0.8 descent results can be expected.


\section{Architecture}
This Section gives a short overview of the tools and architecture used to implement KantNER. The artifacts involved are shown in Figure \ref{architecture}. To achieve the goals described in the introductory chapter there are four main steps. First a complete list of all persons with the corresponding text passages is needed to verify the results of the NER model by calculating the F-Score. A list of persons appearing in the works of Kant is scraped from an online index of persons with the Python library \textit{BeatyfulSoup}, shown in Figure \ref{architecture} by the yellow process b. Then a text search is performed on the Cpr to get every occurrence of the manes from the index. Those are the true named entities displayed by the green tin in Figure \ref{architecture} resulting from process c. Steps b and c will be described in detail in chapter \ref{named entities}.

\begin{figure}[!htbp]
    \centering
    \includegraphics[width=120mm]{images/kantner_architecture.png}
    \caption{Architecture\label{architecture}}
\end{figure}

\noindent
Third the SpaCy NER model is run on the DTA version of Cpr to get the NER results (see process a in Figure \ref{architecture}). As some of the persons that were detected by the NER model were not listed in the index of persons, a feedback loop was established. The artifact resulting from process a, further explained in section \ref{running_the_ner_model} are the NER results. which are tested against the true named entities resulting from the first and second steps. Finally the F-Score is calculated from the results of the NER model and the real named entities. The results will be analyzed in chapter \ref{results}. 
 

\section{Finding all Named Entities} \label{named entities}
This section explains what is the data stored inside the yellow and green tin in Figure \ref{architecture}. Figure \ref{er_person} shows the data retrieved from the index of persons by \textit{Korpora.org}. It was persisted in a \textit{sqlite} database according to the \textit{entity-relationship-diagram} (ER-diagram). As a starting point I used the index of persons provided by \textit{Korpora.org}, assuming all persons or at least the ones considered important that are mentioned in a certain work are listed in the index. An index of persons is intended to be used to look up a person and then get a reference to a part of the text that is about that person. Thus not every occurrence of a person is listed in the index. The table 'mention' contains the entries scraped from the index of persons, referring to the person that was mentioned and the work they are mentioned in (see tables person, mention and work in figure \ref{er_person}). Also persons are not always called by the same name, i.e. persons names have synonyms. That reflects in the index such as there are entries like 'Arnoldi see Bacon' \citep{korpora2019persindex} which means 'Arnoldi' is a synonym of Francis Bacon. Synonyms are also the full name and just the last name of a person, since Fracis Bacon could be referred to as 'Bacon' too. This leads to a one-to-many relation of the tables 'person' and 'synonym' as shown in figure \ref{er_person}. The data described so far is represented by the yellow tin in Figure \ref{architecture}. 

\begin{figure}[!htbp]
\centering
\includegraphics[width=60mm]{images/er_person.png}
\caption{ER Diagram Named Entities\label{er_person}}
\end{figure}

\noindent
Finally from a full text search of all synonyms all occurrences of the persons in Cpr were retrieved. Those are considered the true named entities that ideally should be found by the NER model. The results of the full text search are stored in the table 'named entities' shown in figure \ref{er_person} and represented by the green tin in figure \ref{architecture}. But at the first try the numbers did not always match the expectation. There should be at least as many true named entities found as there are mention according to the index of persons. When that was not the case, I took a closer look at the data and found some alternative spellings Kant used, e.g. Leibniz should have at least 12 hits but was only found one time. When the synonym 'Leibnitz' was added 16 occurrences were found. The results from the NER proved the list still incomplete as will be described further in the following chapter.


\section{Running the NER model} \label{running_the_ner_model}
Running the NER model on the preprocessed version of Cpr a brief review of the results suggested some post-processing steps to gain higher quality results. Among the initially 1204 results identified as person were 133 starting with a lowercase letter. Since person names always start uppercase, those results were filtered out. Another even bigger amount of results were parts of the title of the current chapter printed on the top of every site. After assuring that the names of the chapters do not contain any person names the results for person named entities could be further reduced to 447. Going through these results some persons names were found that did not occur in the index of persons and therefore were not among the true named entities. Examples are Kant himself, god and Hekabe. Additional synonyms like 'Epicur' as an alternative spelling of Epikur were detected and manually added as synonyms as well. This process is shown in figure \ref{architecture} as the feedback loop from the NER results to the yellow tin and resulted in 92 true named entities. Figure \ref{er_full} shows the complete ER-diagram with the tables 'ner\_result', 'text\_version' and 'f\_score'.


\begin{figure}[!htbp]
\centering
\includegraphics[width=100mm]{images/er_full.png}
\caption{ER Diagram with F-Score\label{er_full}}
\end{figure}

\noindent
The table 'text\_version' tracks on which specific version of an edition the results were based. The output of the NER model that was run on a text version are persisted in the table 'ner\_result'. The line in which the supposed named entity was detected was persisted for further manual exploration of the results. From a comparison of the true named entities and the results of the NER model by the start- and end-character of the words of interest the F-Score was calculated along other values in the table 'f\_score'. With this setup it is possible to run the NER model on other versions of the first edition of 'Cpr'. How the F-Score was calculated and how well the NER model performed is explained in the following chapter.


\section{Results} \label{results}
Aside from the question weather current-state NER models could be used for applications like a smart text edition, the aim of this paper is to evaluate the performance of the \textit{de\_core\_news\_md-2.2.0} model by calculating the F-Score. Or in other words: "\textit{How well did the model perform on that old fashioned text it was not even trained for?}" An overview of the entities needed for the calculation is given in figure \ref{f_score}.

\begin{figure}[!htbp]
\centering
\includegraphics[width=70mm]{images/venn_recall_precision.png}
\caption{F-Score\label{f_score}}
\end{figure}

\noindent
In figure \ref{f_score} the green rectangle represents the 92 true named entities that were found through the process described in chapter \ref{named entities}. The blue circle stands for the 447 results found by the NER model. They can be divided into two groups. True positives are the 32 desired results that are in fact named entities. The 415 results that the NER model faulty suspects to be named entities are called false positives. False negatives are the 60 true named entities that were not found by the NER model. The number of the desired results that were actually found by the NER model as the ratio of correct results to all results is called Precision. It is calculated as follows:

\begin{center}
$Precision = \frac{true\ positives}{NER\ results} = \frac{32}{447} = 0.07159$
\end{center}

\noindent
The model did not perform well on Precision, due to the 447 results and would have done even worse with the initially 1204 results that were reduced through post-processing as described in the previous chapter. One big problem seems to be that the model detects a lot of words as supposed named entities that are just normal words. But how many of all named entities were found by NER? The ratio of results found by NER which are named entities to all named entities is called Recall:

\begin{center}
$Recall = \frac{true\ positives}{named\ entities} = \frac{32}{92} = 0.3478$
\end{center}

\noindent
With about one third of the true named entities were found by the model the Recall was better than the Precision. To bring the two measures into one value the F-Score sets them into proportion:

\begin{center}
$F-Score = 2* \frac{Precision\ *\ Recall}{Precision\ +\ Recall} = 0.1187$
\end{center}

\noindent
In comparison to the values published by the creators of SpaCy, the model performs not nearly as good on the 'Cpr' than on the 'WikiNER' corpus as shown in table \ref{f_score_compared}.
	
	\begin{table}[H]
	    \centering
	    \begin{tabular}{ l | c | c | c}
	          & Recall & Precision & F-Score \\ \hline
	        WikiNER & 0.8266 & 0.8357 & 0.8311 \\ \hline
	        Cpr & 0.3478 & 0.0716 & 0.1187 \\
	    \end{tabular}
	    \caption{Compared F-Scores}
	    \label{f_score_compared}
	\end{table}
	
\noindent
The difference in performance is presumably grounded in the different forms of text the model was trained on and applied to. Kant's 16th century German is different to todays news and Wikipedia articles. Finding a third of the persons in the text could be seen as a descent result but the low Precision makes it not applicable to completely automated real life tasks within the field of Digital Humanities without further adaption of the model.


\section{Conclusion}
KantNER ca be seen as productive in two respects. First the artifact of a nearly complete digital collection of the persons mentioned in Kant's Critique of pure reason that could be used in practice. Second it proves the viewpoint of real life applications of NER a suitable benchmark for the detection of flaws in existing models. Providing valuable critique can lead to new impulses for the development of NER models. The synergy between an index of persons that was created manually and never aimed for completion and a pretty new technique of NER using a neural network with a lot of unwanted data as output finally led to a more satisfying result than one of the approaches could have produced on their own.

%2.) Better F-Score\\
Current NER models are not up to the task of fully automated real life applications in humanities. A higher F-Score must be achieved in order to increase the level of automation. It was shown that automated post-processing like filtering out lowercase words from German named entities leads to better results. But to increase the performance of the NER models on this use case,
the models must be trained on data closer to use cases in DH. A lot of models are trained with data from social media but not so much on philosophical texts or literature. The variety of genres used for training should be increased. The training data should be diversified and specialized on other dimensions, e.g. time. Just like temporal word embeddings, NER models could be created to work well on texts from a certain time period. Also the training with additional types of named entities can open new perspectives. Being able to detect mentions of a work from another author in a text, e.g. with the label 'Book' can be seen as a desirable goal from the viewpoint of DH.

%3.) What Humantities\\
Humanists can contribute to the development of NER models as well. To reach the above proposed developments in NER, additional and more specific training data is necessary. Though it is pretty easy to download a dump of the Wikipedia or a big chunk of Twitter messages, finding digitalized material from fields of humanities can be a challenge. The contribution from the communities of humanities can be to continue to open up to the digital realm. The more digital material is available the better the results of techniques like NER will get. Humanists must describe requirements for real life applications of DH that can aid their research. As long as techniques from the field of DH do not allow fully automated processes to build high quality tools, the approach of \textit{Citizen Science} (CS) can be useful to conquer the manual steps including quality assurance. Neural networks ultimately base on statistics and some approaches don't even produce deterministic outcomes. For the application in humanities the results must be trustworthy. Therefore manual review might never become obsolete. 

%4.) Coop\\%1.) Project \& Extensions \\
The experiences from the KantNER project suggest widening the aim of research in NER to real life applications in the practice of humanities can prove fruitful for both fields. But KantNER could already be further enhanced. Other NER models could be tested on how they perform on Critique of pure reason. Including more works of Kant or different types of named entities will lead to more test data. It will also provide the data for a smart online edition of the works of Immanuel Kant. The list of persons in Cpr generated by KantNER might be the more complete than any index of persons online. In a smart text edition the named entities would be provided with additional information, e.g. through a hyperlink to the corresponding Wikipedia article. Readers would then be able to hover over the word 'Hecuba' and end up learning about Hekabe. Since the process to create the list of true named entities might have missed some occurrences of persons, it should be reviewed by humanists. The approach of citizen science could help to find possible errors and missing synonyms. The combination of a public smart text edition with review through citizen science could lead to high quality editions useful to humanists and simultaneously help to accumulate high quality training data for neural networks. It is still a long way to go to reach the ideal of a complete corpus of linked smart editions of all books ever written. This project can hopefully show some steps towards that.


%\Literaturverzeichnis
\newpage
\pagenumbering{roman}
\printbibliography[heading=bibintoc, title={Bibliography}]

\end{document}
