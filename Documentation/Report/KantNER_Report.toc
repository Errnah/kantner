\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Table of Content}{i}{section*.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}Text and Model}{2}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Architecture}{3}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Finding all Named Entities}{3}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Running the NER model}{5}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Results}{5}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Conclusion}{7}{section.7}
\defcounter {refsection}{0}\relax 
\contentsline {section}{Bibliography}{i}{section*.2}
